using Assets.Scripts;
using UnityEngine;

public class BottleController : MonoBehaviour
{
    public void OnEnable()
    {
    }

    // Update is called once per frame
    private void Update()
    {
        if (GameManager.Instance.IsPaused) return;
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(name + " collided with " + other.name);
        if (other.GetComponent<ShipController>() != null)
        {
            GameEvents.BottleCollected();
        }
    }
}
