﻿using System;
using UnityEngine;

namespace Assets.Scripts
{
    public class GameEvents : MonoBehaviour
    {
        public static event Action OnBottleCollected;

        public static event Action OnStartGame;
        public static event Action OnRetryLevel;
        public static event Action OnLevelComplete;

        public static event Action OnCreatureCollected;
        public static event Action<int> OnDamageShip;
        public static event Action OnShipDamaged;
        public static event Action OnShipDestroyed;
        public static event Action OnGameComplete;
        public static event Action OnContinue;

        public static void CreatureCollected() => OnCreatureCollected?.Invoke();
        public static void DamageShip(int amount) => OnDamageShip?.Invoke(amount);
        public static void ShipDamaged() => OnShipDamaged?.Invoke();
        public static void Continue() => OnContinue?.Invoke();
        public static void ShipDestroyed() => OnShipDestroyed?.Invoke();
        public static void StartGame() => OnStartGame?.Invoke();
        public static void RetryLevel() => OnRetryLevel?.Invoke();
        public static void LevelComplete() => OnLevelComplete?.Invoke();
        public static void GameComplete() => OnGameComplete?.Invoke();
        public static void BottleCollected() => OnBottleCollected?.Invoke();
    }
}