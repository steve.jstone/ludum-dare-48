using UnityEngine;
using UnityEngine.UIElements;

public class HudController : MonoBehaviour
{
    [SerializeField] private Sprite _heartFullImage;
    [SerializeField] private Sprite _heartEmptyImage;

    private VisualElement _root;

    public void OnEnable()
    {
        _root = GetComponent<UIDocument>().rootVisualElement;
    }

    public void Hide()
    {
        _root.Q<VisualElement>("GameBackground").style.display = DisplayStyle.None;
    }

    public void Show()
    {
        _root.Q<VisualElement>("GameBackground").style.display = DisplayStyle.Flex;
    }

    public void SetHealth(int amount)
    {
        var maxHealth = 5;
        for (int heartNumber = 1; heartNumber <= maxHealth; heartNumber++)
        {
           var heart = _root.Q<VisualElement>("Heart" + heartNumber);
            if (amount < heartNumber)
            {
                heart.style.backgroundImage = _heartEmptyImage.texture;
            } else
            {
                heart.style.backgroundImage = _heartFullImage.texture;
            }
        }
    }

    public void SetCollectionNumber(int current, int max)
    {
        _root.Q<Label>("CurrentCollectables").text = current.ToString();
        _root.Q<Label>("MaxCollectables").text = max.ToString();
    }

    internal void SetHealth(object currentHealth)
    {
        throw new System.NotImplementedException();
    }
}
