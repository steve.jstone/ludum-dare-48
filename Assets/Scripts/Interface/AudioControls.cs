﻿using System;
using UnityEngine;
using UnityEngine.UIElements;

namespace Assets.Scripts.Interface
{
    [RequireComponent(typeof(UIDocument))]
    public class AudioControls : MonoBehaviour
    {
        [SerializeField] private Sprite _soundIcon;
        [SerializeField] private Sprite _soundIconMuted;

        [Header("References")]
        [SerializeField] private AudioManager _audioManager;

        private VisualElement _root;

        public void OnEnable()
        {
            _root = GetComponent<UIDocument>().rootVisualElement;

            _root.Q<Button>("SoundToggle").RegisterCallback<ClickEvent>(OnToggleSoundClicked);
            _root.Q<Button>("MusicToggle").RegisterCallback<ClickEvent>(OnToggleMusicClicked);

            UpdateIcons();
        }

        private void UpdateIcons()
        {
            UpdateIcon(_audioManager.SoundMuted, "SoundToggle");
            UpdateIcon(_audioManager.MusicMuted, "MusicToggle");
        }

        private void OnToggleSoundClicked(ClickEvent evt)
        {
            _audioManager.ToggleSound();
            UpdateIcon(_audioManager.SoundMuted, "SoundToggle");
        }

        private void UpdateIcon(bool muted, string buttonName)
        {
            if (muted)
            {
                _root.Q<Button>(buttonName).style.backgroundImage = _soundIconMuted.texture;
            }
            else
            {
                _root.Q<Button>(buttonName).style.backgroundImage = _soundIcon.texture;
            }
        }

        private void OnToggleMusicClicked(ClickEvent evt)
        {
            _audioManager.ToggleMusic();
            UpdateIcon(_audioManager.MusicMuted, "MusicToggle");
        }
    }
}