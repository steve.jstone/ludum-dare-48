using Assets.Scripts;
using UnityEngine;
using UnityEngine.UIElements;

[RequireComponent(typeof(UIDocument))]
public class ScreenController : MonoBehaviour
{
    [SerializeField] private int _fadeDuration = 300;
    private VisualElement _root;

    private void OnEnable()
    {
        _root = GetComponent<UIDocument>().rootVisualElement;

        _root.RegisterCallback<GeometryChangedEvent>(OnGeometryChanged);
    }

    private void OnGeometryChanged(GeometryChangedEvent evt)
    {
        _root.Q<VisualElement>("Background").style.opacity = 0;
        _root.Q<VisualElement>("Background").style.display = DisplayStyle.None;
        _root.Q<Button>("StartButton")?.RegisterCallback<ClickEvent>(OnStartClicked);
        _root.Q<Button>("RetryButton")?.RegisterCallback<ClickEvent>(OnRetryClicked);
        _root.Q<Button>("ContinueButton")?.RegisterCallback<ClickEvent>(OnContinueClicked);
        _root.UnregisterCallback<GeometryChangedEvent>(OnGeometryChanged);
    }

    private void OnContinueClicked(ClickEvent evt)
    {
        FadeOut();
        GameEvents.Continue();
    }

    private void OnRetryClicked(ClickEvent evt)
    {
        FadeOut();
        GameEvents.RetryLevel();
    }

    private void OnDisable()
    {
        _root.Q<Button>().UnregisterCallback<ClickEvent>(OnStartClicked);
    }

    private void OnStartClicked(ClickEvent evt)
    {
        FadeOut();
        GameEvents.StartGame();
    }

    private void FadeOut()
    {
        var background = _root.Q<VisualElement>("Background");
        background.experimental.animation.Start(1, 0, _fadeDuration, (element, value) =>
        {
            element.style.opacity = value;
        }).OnCompleted(() =>
        {
            background.style.display = DisplayStyle.None;
        });
    }

    private void FadeIn()
    {
        var background = _root.Q<VisualElement>("Background");
        background.style.display = DisplayStyle.Flex;
        background.experimental.animation.Start(0, 1, _fadeDuration, (element, value) =>
        {
            element.style.opacity = value;
        });
    }

    public void Show()
    {
        _root.Q<VisualElement>("Background").style.opacity = 0;
        FadeIn();
    }

    public void Hide()
    {
        FadeOut();
    }

    public void SetTitle(string text)
    {
        _root.Q<Label>("TitleText").text = text;
        _root.MarkDirtyRepaint();
    }
}
