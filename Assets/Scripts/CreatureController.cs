using Assets.Scripts;
using UnityEngine;

public class CreatureController : MonoBehaviour
{
    [SerializeField] private float _maxSpeed = 1f;
    [SerializeField] private float _currentSpeed = .5f;

    private ICreatureBehaviour _behaviour;

    public void OnEnable()
    {
        _behaviour = GetComponent<ICreatureBehaviour>();
    }

    // Update is called once per frame
    private void Update()
    {
        if (GameManager.Instance.IsPaused) return;

        var newPosition = transform.position += transform.forward * _currentSpeed * Time.deltaTime;

        var worldBounds = GameManager.Instance.WorldBounds;
        if (transform.position.x > worldBounds.x / 2) newPosition.x -= worldBounds.x;
        if (transform.position.z > worldBounds.z / 2) newPosition.z -= worldBounds.z;
        if (transform.position.x < -worldBounds.x / 2) newPosition.x += worldBounds.x;
        if (transform.position.z < -worldBounds.z / 2) newPosition.z += worldBounds.z;

        transform.position = newPosition;
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(name + " collided with " + other.name);
        if (other.GetComponent<ShipController>() != null)
        {
            _behaviour.OnShipCollision();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        Debug.Log(name + " stopped colliding with " + other.name);
        if (other.GetComponent<ShipController>() != null)
        {
            _behaviour.OnShipStoppedColliding();
        }
    }
}
