using System;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    [SerializeField] private AudioClip _backgroundMusic;
    [SerializeField] private AudioClip _collectSound;
    [SerializeField] private AudioClip _levelCompleteSound;
    [SerializeField] private AudioClip _gameOverSound;
    [SerializeField] private AudioClip _shipDamagedSound;

    [SerializeField] private AudioSource _soundSource;
    [SerializeField] private AudioSource _musicSource;

    public bool SoundMuted => _soundSource.mute;
    public bool MusicMuted => _musicSource.mute;

    public void StartMusic()
    {
        _musicSource?.Play();
    }

    public void PlayCollectSound()
    {
        _soundSource?.PlayOneShot(_collectSound);   
    }

    public void PlayLevelCompleteSound()
    {
        _soundSource?.PlayOneShot(_levelCompleteSound);
    }

    internal void ToggleSound()
    {
        _soundSource.mute = !_soundSource.mute;
    }

    internal void ToggleMusic()
    {
        _musicSource.mute = !_musicSource.mute;
    }

    public void PlayGameOverSound()
    {
        _soundSource?.PlayOneShot(_gameOverSound);
    }

    public void PlayShipDamagedSound()
    {
        _soundSource?.PlayOneShot(_shipDamagedSound);
    }
}
