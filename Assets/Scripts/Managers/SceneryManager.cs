using Assets.Scripts.Data;
using UnityEngine;

public class SceneryManager : MonoBehaviour
{
    [SerializeField] private GameObject _sea;
    [SerializeField] private GameObject _deepSea;

    [Header("References")]
    [SerializeField] private Light _directionalLight;

    public void UpdateScenryForLevel(Level level, float lightIntensity, Color lightColor, Color ambientColor)
    {
        if (level.DeepSea)
        {
            _sea.SetActive(false);
            _deepSea.SetActive(true);
        }
        else
        {
            _sea.SetActive(true);
            _deepSea.SetActive(false);
        }

        _directionalLight.intensity = lightIntensity;
        _directionalLight.color = lightColor;
        RenderSettings.ambientLight = ambientColor;
    }
}
