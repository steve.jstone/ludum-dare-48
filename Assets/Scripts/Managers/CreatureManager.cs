using Assets.Scripts;
using Assets.Scripts.Data;
using System.Collections.Generic;
using UnityEngine;

public class CreatureManager : MonoBehaviour
{
    private List<CreatureController> _creatures = new List<CreatureController>();

    public void SpawnCreaturesForLevel(Level level)
    {
        foreach(var creature in _creatures)
        {
            if (creature != null)
            {
                Destroy(creature.gameObject);
            }
        }
        _creatures.Clear();

        var worldBounds = GameManager.Instance.WorldBounds;
        SpawnCreatures(worldBounds, level.HazardCount, level.Hazards);
        SpawnCreatures(worldBounds, level.CollectableCount, level.Collectables);
    }

    private void SpawnCreatures(Vector3 worldBounds, int amount, List<GameObject> prefabs)
    {
        for (int i = 0; i < amount; i++)
        {
            var creatureIndex = Random.Range(0, prefabs.Count);

            var position = PositionHelpers.GetRandomPosition(worldBounds);

            var rotation = Quaternion.Euler(0, Random.Range(0, 360), 0);

            var creature = Instantiate(prefabs[creatureIndex], position, rotation);
            _creatures.Add(creature.GetComponent<CreatureController>());
        }
    }

    public void OnDisable()
    {
        _creatures.Clear();
    }
}
