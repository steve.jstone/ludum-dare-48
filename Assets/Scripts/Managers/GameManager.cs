using Assets.Scripts;
using Assets.Scripts.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    [SerializeField] private Vector3 _worldBounds = new Vector3(10, 10, 10);
    [SerializeField] private List<Level> _levels;
    [SerializeField] private ShipController _ship;

    [Header("Managers")]
    [SerializeField] private CreatureManager _creatureManager;
    [SerializeField] private SceneryManager _sceneryManager;
    [SerializeField] private AudioManager _audioManager;

    [Header("Screens")]
    [SerializeField] private ScreenController _titleScreen;
    [SerializeField] private ScreenController _victoryScreen;
    [SerializeField] private ScreenController _gameOverScreen;
    [SerializeField] private ScreenController _levelCompleteScreen;
    [SerializeField] private HudController _hud;

    [Header("References")]
    [SerializeField] private LightPresets _lightPresets;
    [SerializeField] private BottleController _bottleController;
    [SerializeField] private QuestionList _questions;

    private int _currentLevelIndex;
    private bool _isPaused;

    private int _currentCollectableCount = 0;

    public Vector3 WorldBounds => _worldBounds;
    public bool IsPaused => _isPaused;

    private void OnEnable()
    {
        _isPaused = true;

        GameEvents.OnShipDestroyed += OnShipDestroyed;
        GameEvents.OnStartGame += OnStartGame;
        GameEvents.OnCreatureCollected += OnCreatureCollected;
        GameEvents.OnBottleCollected += OnBottleCollected;
        GameEvents.OnLevelComplete += OnLevelComplete;
        GameEvents.OnGameComplete += OnGameComplete;
        GameEvents.OnShipDamaged += OnShipDamaged;
        GameEvents.OnRetryLevel += OnRetryLevel;
        GameEvents.OnContinue += OnContinue;
    }

    private void OnDisable()
    {
        GameEvents.OnShipDestroyed -= OnShipDestroyed;
        GameEvents.OnStartGame -= OnStartGame;
        GameEvents.OnCreatureCollected -= OnCreatureCollected;
        GameEvents.OnBottleCollected -= OnBottleCollected;
        GameEvents.OnLevelComplete -= OnLevelComplete;
        GameEvents.OnGameComplete -= OnGameComplete;
        GameEvents.OnShipDamaged -= OnShipDamaged;
        GameEvents.OnRetryLevel -= OnRetryLevel;
        GameEvents.OnContinue -= OnContinue;
    }

    private void OnShipDamaged()
    {
        if (_ship.IsAlive)
        {
            _audioManager.PlayShipDamagedSound();
        }
        _hud.SetHealth(_ship.CurrentHealth);
    }

    private IEnumerator Start()
    {
        _audioManager.StartMusic();

        _hud.Hide();
        _titleScreen.gameObject.SetActive(true);
        _victoryScreen.gameObject.SetActive(true);
        _gameOverScreen.gameObject.SetActive(true);
        _levelCompleteScreen.gameObject.SetActive(true);
        _ship.ToggleLight(false);
        yield return null;
        _titleScreen.Show();
    }


    private void OnRetryLevel()
    {
        _currentCollectableCount = 0;
        _ship.ResetShip();
        StartLevel();
        _isPaused = false;
    }

    private void OnGameComplete()
    {
        _hud.Hide();
        _victoryScreen.Show();
        _isPaused = true;
    }

    private void OnLevelComplete()
    {
        _audioManager.PlayLevelCompleteSound();
        _hud.Hide();
        _isPaused = true;
        _levelCompleteScreen.SetTitle(_questions.Items[_currentLevelIndex]);
        _levelCompleteScreen.Show();
    }

    private void OnCreatureCollected()
    {
        _audioManager.PlayCollectSound();
        _currentCollectableCount++;
        var level = GetCurrentLevel();
        _hud.SetCollectionNumber(_currentCollectableCount, level.CollectableCount);
        if (_currentCollectableCount >= level.CollectableCount)
        {
            _bottleController.transform.position = PositionHelpers.GetRandomPosition(WorldBounds);
            _bottleController.gameObject.SetActive(true);
        }
    }

    private void OnBottleCollected()
    {
        _bottleController.gameObject.SetActive(false);
        GameEvents.LevelComplete();
    }

    private void OnStartGame()
    {
        _currentLevelIndex = 0;
        _currentCollectableCount = 0;
        _ship.ResetShip();
        StartLevel();
        _isPaused = false;
    }

    private void OnShipDestroyed()
    {
        _isPaused = true;
        _gameOverScreen.Show();
        _audioManager.PlayGameOverSound();
    }

    private void OnContinue()
    {
        if (_currentLevelIndex >= _levels.Count - 1)
        {
            GameEvents.GameComplete();
        }
        else
        {
            _currentLevelIndex++;
            StartLevel();
            _isPaused = false;
        }
    }

    private Level GetCurrentLevel()
    {
        return _levels[_currentLevelIndex];
    }

    private void StartLevel()
    {
        _currentCollectableCount = 0;
        var level = GetCurrentLevel();
        _creatureManager.SpawnCreaturesForLevel(level);
        _ship.ToggleLight(level.DeepSea);
        _ship.RestoreHealth();

        _hud.SetCollectionNumber(0, level.CollectableCount);
        _hud.SetHealth(_ship.CurrentHealth);
        _hud.Show();

        var levelPosition = (float)_currentLevelIndex / ((float)_levels.Count - 1);
        var intensity = _lightPresets.LightIntensity.Evaluate(levelPosition);
        var color = _lightPresets.LightColor.Evaluate(levelPosition);
        var ambient = _lightPresets.AmbientColor.Evaluate(levelPosition);

        _sceneryManager.UpdateScenryForLevel(level, intensity, color, ambient);
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(transform.position, _worldBounds);
    }
}
