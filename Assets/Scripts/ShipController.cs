using Assets.Scripts;
using System;
using UnityEngine;
using UnityEngine.InputSystem;

public class ShipController : MonoBehaviour
{
    [Header("Movement")]
    [SerializeField] private float _maxSpeed = 1f;
    [SerializeField] private float _currentSpeed = .5f;
    [SerializeField] private float _rotationSpeed = 1f;
    [SerializeField] private float _acceleration = 1f;

    [Header("Health")]
    [SerializeField] private int _maxHealth = 5;
    [SerializeField] private int _currentHealth = 5;

    [Header("References")]
    [SerializeField] private Light _light;

    private Vector2 _moveVector;
    public int CurrentHealth => _currentHealth;

    public bool IsAlive => _currentHealth > 0;

    private void OnEnable()
    {
        _currentHealth = _maxHealth;

        GameEvents.OnDamageShip += OnReceiveDamage;
    }

    private void OnDisable()
    {
        GameEvents.OnDamageShip -= OnReceiveDamage;
    }

    private void OnReceiveDamage(int amount)
    {
        _currentHealth--;

        GameEvents.ShipDamaged();
        if (_currentHealth <= 0)
        {
            GameEvents.ShipDestroyed();
        }
    }

    public void ToggleLight(bool active)
    {
        _light.gameObject.SetActive(active);
    }

    public void Move(Vector2 vector2)
    {
        _moveVector = vector2;
    }

    private void Update()
    {
        if (GameManager.Instance.IsPaused) return;

        var rotationMultiplier = Mathf.Lerp(.5f, 1.5f, 1f - ((float)_currentSpeed / (float)_maxSpeed));
        var rotationSpeed = _rotationSpeed * rotationMultiplier;

        if (_moveVector.x > 0)
        {
            transform.Rotate(Vector3.up, rotationSpeed * Time.deltaTime, Space.Self);
        }
        else if (_moveVector.x < 0)
        {
            transform.Rotate(Vector3.up, -rotationSpeed * Time.deltaTime, Space.Self);
        }

        if (_moveVector.y > 0)
        {
            _currentSpeed += _acceleration * Time.deltaTime;
        }
        else if (_moveVector.y <0)
        {
            _currentSpeed -= _acceleration * Time.deltaTime;
        }

        _currentSpeed = Mathf.Clamp(_currentSpeed, 0, _maxSpeed);

        var newPosition = transform.position + transform.forward * _currentSpeed * Time.deltaTime;

        var worldBounds = GameManager.Instance.WorldBounds;
        if (transform.position.x > worldBounds.x / 2) newPosition.x -= worldBounds.x;
        if (transform.position.z > worldBounds.z / 2) newPosition.z -= worldBounds.z;
        if (transform.position.x < -worldBounds.x / 2) newPosition.x += worldBounds.x;
        if (transform.position.z < -worldBounds.z / 2) newPosition.z += worldBounds.z;

        transform.position = newPosition;
    }

    public void ResetShip()
    {
        _currentHealth = _maxHealth;
        transform.position = new Vector3(0, transform.position.y, 0);
        transform.rotation = Quaternion.identity;
    }

    internal void RestoreHealth()
    {
        _currentHealth = _maxHealth;
    }
}
