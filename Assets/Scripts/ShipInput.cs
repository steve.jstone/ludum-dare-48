using UnityEngine;
using UnityEngine.InputSystem;

public class ShipInput : MonoBehaviour
{
    [SerializeField] private ShipController _shipController;

    public void OnMove(InputValue value)
    {
        _shipController.Move(value.Get<Vector2>());
    }
}
