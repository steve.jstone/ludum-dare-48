﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts
{
    public class PositionHelpers
    {
        public static Vector3 GetRandomPosition(Vector3 worldBounds)
        {
            return new Vector3(
                Random.Range(-worldBounds.x / 2, worldBounds.x / 2),
                0,
                Random.Range(-worldBounds.z / 2, worldBounds.z / 2));
        }
    }
}