﻿using System.Linq;
using UnityEngine;

public abstract class Singleton<T> : MonoBehaviour where T : Singleton<T>
{
    private static T _instance;
    public static T Instance { 
        get  
        {
            if (_instance == null) {
               var objects = Resources.FindObjectsOfTypeAll<T>();
                _instance = objects.SingleOrDefault();
            }
            return _instance;
        }
    }
    
    private void OnApplicationQuit()
    {
        _instance = null;
    }


    private void OnDestroy()
    {
        _instance = null;
    }
}
