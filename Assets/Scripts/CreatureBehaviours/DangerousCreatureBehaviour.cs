﻿using UnityEngine;

namespace Assets.Scripts
{
    public class DangerousCreatureBehaviour : MonoBehaviour, ICreatureBehaviour
    {
        [SerializeField] private int _damageAmount = 1;
        public void OnShipCollision()
        {
            GameEvents.DamageShip(_damageAmount);
        }

        public void OnShipStoppedColliding()
        {
        }
    }
}