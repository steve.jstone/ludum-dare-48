﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts
{
    public interface ICreatureBehaviour
    {
        public void OnShipCollision();
        public void OnShipStoppedColliding();
    }
}