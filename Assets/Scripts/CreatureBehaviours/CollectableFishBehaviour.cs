﻿using UnityEngine;

namespace Assets.Scripts
{
    public class CollectableFishBehaviour : MonoBehaviour, ICreatureBehaviour
    {
        public void OnShipCollision()
        {
            GameEvents.CreatureCollected();
            Destroy(gameObject);
        }

        public void OnShipStoppedColliding()
        {
        }
    }
}