﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Data
{
    [CreateAssetMenu]
    public class Level : ScriptableObject
    {
        [Header("Creatures")]
        [SerializeField] private int _hazardCount;
        [SerializeField] private int _collectableCount;
        [SerializeField] private List<GameObject> _hazards;
        [SerializeField] private List<GameObject> _collectables;

        [Header("Scenery")]
        [SerializeField] private bool _deepSea = false;

        public int HazardCount => _hazardCount;
        public int CollectableCount => _collectableCount;
        public List<GameObject> Hazards => _hazards;
        public List<GameObject> Collectables => _collectables;
        public bool DeepSea => _deepSea;
    }
}