﻿using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Data
{
    [CreateAssetMenu]
    public class LightPresets : ScriptableObject
    {
        [SerializeField] private AnimationCurve _lightIntensity;
        [SerializeField] private Gradient _lightColor;
        [SerializeField] private Gradient _ambientColor;

        public AnimationCurve LightIntensity => _lightIntensity;
        public Gradient LightColor => _lightColor;
        public Gradient AmbientColor => _ambientColor;
    }
}