﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Data
{
    [CreateAssetMenu]
    public class QuestionList : ScriptableObject
    {
        [SerializeField] private List<string> _items;

        public List<string> Items => _items;
    }
}